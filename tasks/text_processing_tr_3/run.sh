#!/bin/bash

while true; do
  read line;
  [[ -z $line ]] && break;
  tr -s " " <<< "$line"
done