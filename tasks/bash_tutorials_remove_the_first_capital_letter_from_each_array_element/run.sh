#!/bin/bash

declare -a arr

while true; do
  read country;
  [[ -z $country ]] && break;
  arr=("${arr[@]}" "$country")
done

#declare -a arr=( "${arr[@]/[A-Z]*/.}" )  # doesn't work, how to match a group?
#echo "${arr[@]}"

for country in "${arr[@]}"
do
  if [[ "${country:0:1}" =~ [A-Z] ]]; then
    len=${#country}-1
    echo -n ."${country:1:$len}"" "
  else
    echo -n "$country"" "  # TODO: remove last space, add newline
  fi
done