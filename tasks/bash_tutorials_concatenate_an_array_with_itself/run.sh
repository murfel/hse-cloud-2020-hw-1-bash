#!/bin/bash

declare -a arr

while true; do
  read country;
  [[ -z $country ]] && break;
  arr=("${arr[@]}" "$country")
done

triplearr=("${arr[@]}" "${arr[@]}" "${arr[@]}")
echo "${triplearr[@]}"
