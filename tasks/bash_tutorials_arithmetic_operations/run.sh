#!/bin/bash

read expr
res=$(bc <<< "scale=3; $expr * 1000")
echo $res
if [ "$res" -le 0 ]; then
  bc <<< "scale=3; (res+0.5)/1000";
else
  if [ "$res" -eq 0 ]; then
    echo "0.000"
  else
    bc <<< "scale=3; (res-0.5)/1000";
  fi
fi