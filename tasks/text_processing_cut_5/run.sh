#!/bin/bash

while true; do
  read line;
  [[ -z $line ]] && break;
  cut -f 1,2,3 <<< "$line"
done