#!/bin/bash
read n

sum=0
: $((c=0))
while [ $((c)) -ne "$n" ]; do
  read x;
  sum=$((sum+x));
: $((c+=1)); done

echo $(bc <<< "scale=3; $sum / $n")