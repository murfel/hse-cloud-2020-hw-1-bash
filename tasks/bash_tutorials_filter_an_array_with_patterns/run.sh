#!/bin/bash

declare -a arr

while true; do
  read country;
  [[ -z $country ]] && break;
  arr=("${arr[@]}" "$country")
done

declare -a arr=( "${arr[@]/*a*/}" )
declare -a arr=( "${arr[@]/*A*/}" )

for country in "${arr[@]}"
do
  [[ -z $country ]] && continue;
  echo "$country"
done
echo