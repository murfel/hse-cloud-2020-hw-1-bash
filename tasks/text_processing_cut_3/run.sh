#!/bin/bash

while true; do
  read line;
  [[ -z $line ]] && break;
  cut -c2-7 <<< "$line"
done