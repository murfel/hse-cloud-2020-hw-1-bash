#!/bin/bash

while true; do
  read line;
  [[ -z $line ]] && break;
  tr -d "[:lower:]" <<< "$line"
done